package com.n26.services;

import com.n26.model.Statistics;
import com.n26.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static com.n26.model.Statistics.statisticsForTxn;
import static java.util.stream.LongStream.iterate;

/**
 * This service doesn't purge old transactions, only inserts new
 * Internal storage is unbounded
 */
@Component
public class StatisticsService {
    // This map contains statistics for time intervals (quanta)
    // Key of the map is timestamp of the start of the interval
    private final ConcurrentHashMap<Long, Statistics> storage;
    private final Clock clock;
    private final int quantumMillis;

    public StatisticsService(ConcurrentHashMap<Long, Statistics> storage,
                             int quantumMillis,
                             Clock clock) {
        this.storage = storage;
        this.quantumMillis = quantumMillis;
        this.clock = clock;
    }

    @Autowired
    public StatisticsService(@Value("${quantum.millis}") int quantumMillis,
                             Clock clock) {
        this(new ConcurrentHashMap<>(), quantumMillis, clock);
    }

    /**
     * Updates/creates statistics for matching time interval according to transaction timestamp
     *
     * This method is thread-safe and has O(1) space and time complexity
     *
     * Thread-safety is guaranteed by ConcurrentHashMap#compute
     */
    public void submit(Transaction transaction) {
        final long interval = transaction.timestamp.toEpochMilli() - (transaction.timestamp.toEpochMilli() % quantumMillis);
        final Statistics forTxn = statisticsForTxn(transaction);

        storage.compute(interval, (__, existingStats) ->
            Optional.ofNullable(existingStats)
                .map(e -> e.merge(forTxn))
                .orElse(forTxn));
    }

    /**
     * This method has O(1) space and time complexity
     *
     * It combines statistics for all intervals within duration, and number of intervals is always
     * a fixed number equals to {@code duration.toMillis() / quantumMillis}
     */
    public Optional<Statistics> statisticsFor(Duration duration) {
        final long start = clock.instant().minus(duration).toEpochMilli();
        final long startInterval = start - (start % quantumMillis);

        return iterate(startInterval, l -> l + quantumMillis)
            .limit(duration.toMillis() / quantumMillis)
            .mapToObj(b -> Optional.ofNullable(storage.get(b)))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .reduce(Statistics::merge);
    }

    public void deleteTransactions() {
        storage.clear();
    }
}
