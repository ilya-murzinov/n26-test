package com.n26.model;

import java.math.BigDecimal;
import java.util.Objects;

import static java.math.BigDecimal.ZERO;
import static java.math.RoundingMode.HALF_UP;
import static java.util.Objects.hash;

public final class Statistics {

    public final long count;
    public final BigDecimal average;
    public final BigDecimal sum;
    public final BigDecimal min;
    public final BigDecimal max;

    Statistics(long count, BigDecimal sum, BigDecimal min, BigDecimal max) {
        this.count = count;
        this.average = count == 0
                       ? ZERO
                       : sum.divide(BigDecimal.valueOf(count), 2, HALF_UP);
        this.sum = sum;
        this.min = min;
        this.max = max;
    }

    // FIXME: doesn't make any sense, but required for integration tests
    public static Statistics empty() {
        return new Statistics(0, ZERO, ZERO, ZERO);
    }

    public static Statistics statisticsForTxn(Transaction transaction) {
        return new Statistics(1, transaction.amount, transaction.amount, transaction.amount);
    }

    public Statistics merge(Statistics statistics) {
        return new Statistics(
            this.count + statistics.count,
            this.sum.add(statistics.sum),
            statistics.min.compareTo(this.min) < 0 ? statistics.min : this.min,
            statistics.max.compareTo(this.max) > 0 ? statistics.max : this.max);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Statistics that = (Statistics) o;
        return count == that.count &&
            Objects.equals(average, that.average) &&
            Objects.equals(sum, that.sum) &&
            Objects.equals(min, that.min) &&
            Objects.equals(max, that.max);
    }

    @Override
    public int hashCode() {
        return hash(count, average, sum, min, max);
    }

    @Override
    public String toString() {
        return "Statistics{" +
            "count=" + count +
            ", average=" + average +
            ", sum=" + sum +
            ", min=" + min +
            ", max=" + max +
            '}';
    }
}
