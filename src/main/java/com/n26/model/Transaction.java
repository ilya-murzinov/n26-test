package com.n26.model;

import java.math.BigDecimal;
import java.time.Instant;

public final class Transaction {
    public final BigDecimal amount;
    public final Instant timestamp;

    public Transaction(BigDecimal amount, Instant timestamp) {
        this.amount = amount;
        this.timestamp = timestamp;
    }
}
