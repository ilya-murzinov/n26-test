package com.n26.controllers;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.n26.model.Transaction;
import com.n26.services.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.status;
import static org.springframework.http.ResponseEntity.unprocessableEntity;

@RestController
@RequestMapping("/transactions")
public class TransactionsController {

    private final Clock clock;
    private final StatisticsService statisticsService;

    @Autowired
    public TransactionsController(Clock clock, StatisticsService statisticsService) {
        this.clock = clock;
        this.statisticsService = statisticsService;
    }

    @PostMapping
    public ResponseEntity createTransaction(@RequestBody TransactionRequest request) {
        final Transaction transaction;

        try {
            transaction = new Transaction(new BigDecimal(request.amount), Instant.parse(request.timestamp));
        } catch (Exception e) {
            return unprocessableEntity().build();
        }

        if (isInFuture(transaction))
            return unprocessableEntity().build();

        if (isExpired(transaction))
            return noContent().build();

        statisticsService.submit(transaction);
        return status(CREATED).build();
    }

    @DeleteMapping
    public ResponseEntity deleteTransactions() {
        statisticsService.deleteTransactions();
        return ResponseEntity.noContent().build();
    }

    private boolean isExpired(Transaction transaction) {
        return clock.instant().minusSeconds(60).isAfter(transaction.timestamp);
    }

    private boolean isInFuture(Transaction transaction) {
        return clock.instant().isBefore(transaction.timestamp);
    }

    private static class TransactionRequest {

        final String amount;
        final String timestamp;

        @JsonCreator
        public TransactionRequest(@JsonProperty("amount") String amount,
                                  @JsonProperty("timestamp") String timestamp) {
            this.amount = amount;
            this.timestamp = timestamp;
        }
    }
}
