package com.n26.controllers;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.n26.model.Statistics;
import com.n26.services.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.Duration;

import static com.fasterxml.jackson.annotation.JsonFormat.Shape.STRING;
import static com.n26.model.Statistics.empty;
import static java.math.RoundingMode.HALF_UP;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/statistics")
public class StatisticsController {

    @Autowired
    private StatisticsService statisticsService;

    @Value("${statistics.duration.millis}")
    private long duration;

    @GetMapping
    public ResponseEntity<StatisticsResponse> getStatistics() {
        final Statistics statistics = statisticsService.statisticsFor(Duration.ofMillis(duration)).orElse(empty());
        return ok(StatisticsResponse.from(statistics));
    }

    private static class StatisticsResponse {

        public static final int SCALE = 2;

        public final long count;
        @JsonFormat(shape = STRING)
        public final BigDecimal avg;
        @JsonFormat(shape = STRING)
        public final BigDecimal sum;
        @JsonFormat(shape = STRING)
        public final BigDecimal min;
        @JsonFormat(shape = STRING)
        public final BigDecimal max;

        private StatisticsResponse(long count, BigDecimal avg, BigDecimal sum, BigDecimal min, BigDecimal max) {
            this.count = count;
            this.avg = avg;
            this.sum = sum;
            this.min = min;
            this.max = max;
        }

        static StatisticsResponse from(Statistics statistics) {
            return new StatisticsResponse(
                statistics.count,
                statistics.average.setScale(SCALE, HALF_UP),
                statistics.sum.setScale(SCALE, HALF_UP),
                statistics.min.setScale(SCALE, HALF_UP),
                statistics.max.setScale(SCALE, HALF_UP));
        }
    }
}