package com.n26.service;

import com.n26.model.Statistics;
import com.n26.model.Transaction;
import com.n26.services.StatisticsService;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import static com.n26.model.Statistics.statisticsForTxn;
import static java.math.BigDecimal.ONE;
import static java.time.Clock.fixed;
import static java.time.ZoneOffset.UTC;
import static java.util.Optional.empty;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;
import static org.assertj.core.api.Assertions.assertThat;

public class StatisticsServiceTest {

    private static final int QUANTUM_MILLIS = 100;
    private final Instant now = Instant.now();

    @Test
    public void should_calculate_statistics_for_single_transaction() {
        // given
        final StatisticsService service = new StatisticsService(new ConcurrentHashMap<>(), QUANTUM_MILLIS, fixed(now, UTC));
        final Transaction transaction = new Transaction(ONE, now.minusSeconds(1));

        // when
        service.submit(transaction);

        // then
        assertThat(service.statisticsFor(Duration.ofSeconds(1)))
            .isEqualTo(Optional.of(statisticsForTxn(transaction)));
    }

    @Test
    public void should_calculate_statistics_for_multiple_transactions() {
        // given
        final StatisticsService service = new StatisticsService(new ConcurrentHashMap<>(), QUANTUM_MILLIS, fixed(now, UTC));
        final Instant timestamp = now.minusSeconds(1);
        final Random random = new Random();

        final List<Transaction> transactions = range(0, 10)
            .mapToObj(i -> new Transaction(BigDecimal.valueOf(i * 100), timestamp.plusMillis(random.nextInt(QUANTUM_MILLIS))))
            .collect(toList());

        // when
        transactions.forEach(service::submit);

        // then
        final Optional<Statistics> expected = transactions
            .stream()
            .map(Statistics::statisticsForTxn)
            .reduce(Statistics::merge);

        assertThat(service.statisticsFor(Duration.ofSeconds(1)))
            .isEqualTo(expected);
    }

    @Test
    public void should_not_consider_old_transactions() {
        // given
        final StatisticsService service = new StatisticsService(new ConcurrentHashMap<>(), QUANTUM_MILLIS, fixed(now, UTC));
        final Transaction transaction = new Transaction(ONE, now.minusSeconds(1).minusMillis(QUANTUM_MILLIS));

        // when
        service.submit(transaction);

        // then
        assertThat(service.statisticsFor(Duration.ofSeconds(1)))
            .isEqualTo(empty());
    }
}
