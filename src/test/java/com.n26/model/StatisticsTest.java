package com.n26.model;

import org.junit.Test;

import java.math.BigDecimal;
import java.time.Instant;

import static com.n26.model.Statistics.statisticsForTxn;
import static org.assertj.core.api.Assertions.assertThat;

public class StatisticsTest {

    private final Instant now = Instant.now();

    private final Statistics statistics =
        new Statistics(
            23,
            BigDecimal.valueOf(169),
            BigDecimal.valueOf(100),
            BigDecimal.valueOf(12));

    private final Statistics forTxn = statisticsForTxn(new Transaction(BigDecimal.valueOf(42), now));

    @Test
    public void should_calculate_average() {
        assertThat(statistics.average).isEqualTo(BigDecimal.valueOf(7.35));
    }

    @Test
    public void should_merge_statistics() {
        final Statistics actual = statistics.merge(forTxn);
        final Statistics expected = new Statistics(
            24,
            BigDecimal.valueOf(169 + 42),
            BigDecimal.valueOf(42),
            BigDecimal.valueOf(42));

        assertThat(actual).isEqualTo(expected);
        assertThat(actual.average).isEqualTo(BigDecimal.valueOf(8.79));
    }

    @Test
    public void merge_should_be_commutative() {
        assertThat(statistics.merge(forTxn)).isEqualTo(forTxn.merge(statistics));
    }
}
